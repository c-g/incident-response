# The Poor Man's Incident Response Toolkit

Tools for incident response which automate evidence collection and some initial analysis

**Dependencies**: `ftkimager lite`, `winpmem`, `SysInternals Tools`

[Download winpmem](http://releases.rekall-forensic.com/)

[Download FTK Imager Lite](https://accessdata.com/product-download)

[Download SysInternals Tools](https://docs.microsoft.com/en-us/sysinternals/downloads/sysinternals-suite)


**Optional**: `GoW` (GNU on Windows), `netcat`, `volatility`, `sleuthkit`, `autopsy`

These tools will help you analyze the data generated by this kit.

[Download GoW](https://github.com/bmatzelle/gow)

[Download netcat](https://nmap.org/ncat/)

[Download volatility](http://www.volatilityfoundation.org/releases)

[Download SleuthKit](https://www.sleuthkit.org/sleuthkit/)

[Download Autopsy](https://www.sleuthkit.org/autopsy/)

[Download Microsoft Message Analyzer](https://www.microsoft.com/en-us/download/details.aspx?id=44226)

## Features

Gathers a great deal of information from a Windows-based system using freely available tools.

- Packet capture.
- Memory image can be captured.
- Disk image can be captured.
- Works with most Windows systems. Limited dependencies. What dependencies exist do not require installation therefore the entire kit is "portable".
- Most data is captured in plain text format for easy analysis using tools such as `grep`. This also allows for fast data transfer over limited bandwidth connections (memory and disk images not-withstanding).
- "Lite" data gathering option to minimize the potential for tipping over a system.

## Instructions

### Directory structure
Create a root directory named whatever you like (e.g. "pmir"). Create sub-directories "sysinternals", "ftkimager", and "winpmem". Copy, extract, or install tools to their respective sub-directories.

### Example Usage
- Place kit on Windows system to analyze and execute `gather-data.bat` **with administrative privileges**.

- Place kit on shared directory, map the drive from the system to be analyzed, and execute `gather-data.bat` **with administrative privileges**.

## Limitations

Some command output is redirected to a temporary cache under `C:\Windows\Temp\export-data`. The data is then copied to the `export_path` you specified at the start of the script execution. At the end of the script during cleanup this directory is removed from the `Windows\Temp` directory.

Currently only supports Windows desktop and server systems.

## Extras
`win_eventids.txt` - Windows Event IDs of interest.

`validate-ad-users.ps1` - PowerShell script to validate that a CSV file of usernames exists in an Active Directory domain.