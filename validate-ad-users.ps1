#$DebugPreference = "Continue"

# Quick PowerShell script to check AD an validate a list of users from a CSV file
# Note the first column must contain the users, with no headers
# Requires the ActiveDirectory cmdlets to be installed/available to validate against

# Created by Chris Goff
# Test-ADAuthentication function from https://stackoverflow.com/a/7664380

# Usage:
# .\validate-ad-users.ps1 <csv file name>

param (
    [Parameter(Mandatory=$true)][string]$filename
)


Function Test-ADAuthentication {
    param($username,$password)
    (new-object directoryservices.directoryentry "",$username,$password).psbase.name -ne $null
}

$CsvWithHeaders = (Import-Csv $filename -header SOURCE,USERNAME,PASSWORD)
$UserList = $CsvWithHeaders.USERNAME
$PasswordList = $CsvWithHeaders.PASSWORD
$domains = '', ''

foreach ($row in $CsvWithHeaders) {
    $u = $row.USERNAME
    $p = $row.PASSWORD
    $Split = $u.Split('@')
    foreach ($domain in $domains) {
        if ($Split[1] -match "^.*?$domain.*?.") {
            Try {
                $ADUser = Get-ADUser -Identity $Split[0]
                Write-Host $ADUser.SamAccountName $p
                Write-Debug "Perform password check."
                $checked = Test-ADAuthentication $ADUser.SamAccountName $p
                if ($checked -eq $true) {
                    Write-Host "Change Password"
                } else {
                    Write-Host "Not a match"
                }
            }
            Catch {
                Write-Warning "Error in matching domain specified."
            }
        }
    }
}